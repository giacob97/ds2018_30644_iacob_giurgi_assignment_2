package server;



import common.interfaces.ICarService;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerStart extends CarService {

    private static final int PORT = 8889;

    public ServerStart() {
    }

    public static void main(String[] args) {
        CarService carService = new CarService();
        try {
            ICarService iCarService = (ICarService) UnicastRemoteObject.exportObject(carService, 0);
            Registry registry = LocateRegistry.createRegistry(PORT);
            registry.bind("ICarService",iCarService);
            System.out.println("Server is ready");
        } catch (RemoteException e) {
            System.out.println("Server exception: " + e.getMessage());
            e.printStackTrace();
        } catch (AlreadyBoundException e) {
            System.out.println("Server exception: " + e.getMessage());
            e.printStackTrace();
        }

    }
}
