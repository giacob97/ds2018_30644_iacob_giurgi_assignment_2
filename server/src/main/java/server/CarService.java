package server;

import common.entities.Car;
import common.interfaces.ICarService;

import java.rmi.RemoteException;

public class CarService implements ICarService {
    public double computeSellingPrice(Car c) throws RemoteException {
        int year = 2018 - c.getYear();
        if(year >= 7){
            return 0.0;
        }
        else{
            double purchasingPrice = c.getPrice();
            return purchasingPrice - ((purchasingPrice/7.0)*year);
        }
    }

    public double computeTax(Car c) throws RemoteException {
        if (c.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngineCapacity() > 1601) sum = 18;
        if(c.getEngineCapacity() > 2001) sum = 72;
        if(c.getEngineCapacity() > 2601) sum = 144;
        if(c.getEngineCapacity() > 3001) sum = 290;
        return c.getEngineCapacity() / 200.0 * sum;
    }
}
