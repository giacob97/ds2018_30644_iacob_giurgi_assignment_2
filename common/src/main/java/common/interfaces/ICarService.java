package common.interfaces;

import common.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICarService extends Remote {


    double computeSellingPrice(Car c) throws RemoteException;
    double computeTax(Car c) throws RemoteException;

}
