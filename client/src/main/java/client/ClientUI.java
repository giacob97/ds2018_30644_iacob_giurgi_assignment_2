package client;

import common.entities.Car;
import common.interfaces.ICarService;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class ClientUI extends Application {


    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("RMI Application");
        GridPane gridPane =this.createGridPane();
        addUIControls(gridPane);
        Scene scene = new Scene(gridPane, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    private GridPane createGridPane() {
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(new Insets(40, 40, 40, 40));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        ColumnConstraints columnOneConstraints = new ColumnConstraints(50, 100, Double.MAX_VALUE);
        columnOneConstraints.setHalignment(HPos.RIGHT);

        ColumnConstraints columnTwoConstrains = new ColumnConstraints(50,100, Double.MAX_VALUE);
        columnTwoConstrains.setHgrow(Priority.ALWAYS);
        gridPane.getColumnConstraints().addAll(columnOneConstraints, columnTwoConstrains);
        return gridPane;
    }

    private void addUIControls(GridPane gridPane) {
        Label headerLabel = new Label("Car Details");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));

        gridPane.add(headerLabel, 0,0,2,1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(20, 0,20,0));


        Label yearLabel = new Label("Year : ");
        gridPane.add(yearLabel, 0,1);


        TextField yearField = new TextField();
        yearField.setPrefHeight(40);
        gridPane.add(yearField, 1,1);



        Label engineCapacityLabel = new Label("Engine Capacity : ");
        gridPane.add(engineCapacityLabel, 0, 2);


        TextField engineCapacityField = new TextField();
        engineCapacityField.setPrefHeight(40);
        gridPane.add(engineCapacityField, 1, 2);


        Label priceLabel = new Label("Price : ");
        gridPane.add(priceLabel, 0, 3);


        TextField priceField = new TextField();
        priceField.setPrefHeight(40);
        gridPane.add(priceField, 1, 3);


        Button submitButton = new Button("Calculate tax");
        submitButton.setPrefHeight(40);
        submitButton.setDefaultButton(true);
        submitButton.setPrefWidth(150);

        Button priceButton = new Button("Calculate selling price");
        priceButton.setPrefHeight(40);
        priceButton.setDefaultButton(true);
        priceButton.setPrefWidth(150);

        gridPane.add(submitButton, 0, 4, 2, 1);
        GridPane.setHalignment(submitButton, HPos.CENTER);
        GridPane.setMargin(submitButton, new Insets(20, 0,20,0));

        gridPane.add(priceButton, 0, 4, 2, 10);
        GridPane.setHalignment(priceButton, HPos.CENTER);
        GridPane.setMargin(priceButton, new Insets(20, 0,20,0));

        submitButton.setOnAction((event) -> {

                if(yearField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter the year");
                    return;
                }
                if(engineCapacityField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter the engine capacity");
                    return;
                }
            try {
                Car c = new Car(Integer.valueOf(yearField.getText()),Integer.valueOf(engineCapacityField.getText()));
                Registry registry = LocateRegistry.getRegistry(8889);
                ICarService carService = (ICarService) registry.lookup("ICarService");
                showAlert(Alert.AlertType.CONFIRMATION, gridPane.getScene().getWindow(), "Registration Successful!", "Tax is: " + carService.computeTax(c));
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (NotBoundException e) {
                e.printStackTrace();
            }
        });
        priceButton.setOnAction((event) -> {
            if(yearField.getText().isEmpty()) {
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter the year");
                return;
            }
            if(engineCapacityField.getText().isEmpty()) {
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter the engine capacity");
                return;
            }
            if(priceField.getText().isEmpty()) {
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Form Error!", "Please enter the price");
                return;
            }
            try {
                Car c = new Car(Integer.valueOf(yearField.getText()),Integer.valueOf(engineCapacityField.getText()),Double.valueOf(priceField.getText()));
                Registry registry = LocateRegistry.getRegistry(8889);
                ICarService carService = (ICarService) registry.lookup("ICarService");
                showAlert(Alert.AlertType.CONFIRMATION, gridPane.getScene().getWindow(), "Registration Successful!", "Selling price is: " + (int)carService.computeSellingPrice(c));
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (NotBoundException e) {
                e.printStackTrace();
            }
        });
    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }



}
